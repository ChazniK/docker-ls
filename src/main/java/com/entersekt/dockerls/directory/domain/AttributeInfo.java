package com.entersekt.dockerls.directory.domain;

import java.util.Date;

public class AttributeInfo {

    private String permissions;
    private String numberItemsWithin;
    private String size;
    private String date;
    private String name;

    public AttributeInfo(String permissions, String numberItemsWithin, String size, String name) {
        this.permissions = permissions;
        this.numberItemsWithin = numberItemsWithin;
        this.size = size;
        this.name = name;
    }

    public void setDate(String month, String day, String time) {

        StringBuilder sb = new StringBuilder();
        this.date = sb.append(time).append(" ").append(day).append(" ").append(month).toString();
    }

    public String getPermissions() {
        return this.permissions;
    }

    public String getNumberItemsWithin() {
        return this.numberItemsWithin;
    }

    public String getSize() {
        return this.size;
    }

    public String getDate() {
        return this.date;
    }

    public String getName() {
        return this.name;
    }
}