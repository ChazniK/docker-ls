package com.entersekt.dockerls.directory.adapter.in.web;

import javax.ws.rs.NotFoundException;

public class RestPreconditions {
    public static <T> T checkFound(T resource) {
        if (resource == null) {
            throw new NotFoundException("path:" + resource.toString());
        }

        return resource;
    }
}
