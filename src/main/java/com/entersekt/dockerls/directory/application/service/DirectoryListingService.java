package com.entersekt.dockerls.directory.application.service;

import com.entersekt.dockerls.directory.application.port.in.DirectoryListingQuery;
import com.entersekt.dockerls.directory.domain.ContainInfo;
import com.entersekt.dockerls.directory.domain.DirectoryListing;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.IOException;

@NoArgsConstructor
@Service
public class DirectoryListingService implements DirectoryListingQuery {

    private ContainerHelperService containerHelperService;
    private String dockerId;

    DirectoryListingService(ContainerHelperService containerHelperService) throws IOException, InterruptedException {
        this.containerHelperService = containerHelperService;
        this.dockerId = containerHelperService.getDockerImage().getId();
    }

    public DirectoryListing getDirectoryListing(String path) throws IOException, InterruptedException {
        if (path == null || path == "") {
            return null;
        } else {
            DirectoryListing directoryListing = containerHelperService.getImageDirectories(path, dockerId);
            if (directoryListing == null) {
                return null;
            }

            return directoryListing;
        }
    }
}


