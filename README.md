# Docker -ls

Restful web service that allows a client application to obtain the full directory listing of a given path in the container.

## Description

Cross-platform program using Java 17 that runs in a Docker container,
exposes a restful interface on port 8080 and allows a client application to
obtain the full directory listing of a given path in the container. This is 
essentially the "ls" command for docker.

## Getting Started

### Dependencies

* Docker

### Installing

* [Install Docker Desktop on Mac](https://docs.docker.com/desktop/mac/install/)
* [Install Docker Desktop on Windows](https://docs.docker.com/desktop/windows/install/)

### Executing program
1. Clone the project to a chosen location
2. Navigate to the project directory
3. Build docker image
4. Check local docker images
5. Run docker container (detached mode)
```
Steps:

git clone https://gitlab.com/ChazniK/docker-ls.git docker-ls

cd docker-ls

docker build --tag directory-docker .

docker images

docker run -d -p 8080:8080 directory-docker

```

### Testing the program

* [Swagger UI](http://localhost:8080/swagger-ui/index.html)

* You man also use Post Man to perform a http get 
```
example http://localhost:8080/directories/{specified path}
```

## Authors

Chazni Katz