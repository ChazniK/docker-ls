package com.entersekt.dockerls.unittests;


import com.entersekt.dockerls.directory.application.service.ContainerHelperService;
import com.entersekt.dockerls.directory.domain.ContainInfo;
import com.entersekt.dockerls.directory.domain.DirectoryListing;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public  class ContainerHelperTests {

    private ContainerHelperService containerHelperService;

    @BeforeEach
    void initUseCase() {
        containerHelperService = new ContainerHelperService();
    }

    @Test
    void getDockerImageNameIsNotEmpty() throws IOException, InterruptedException {

        ContainInfo containInfo = containerHelperService.getDockerImage();
        assertThat(!containInfo.getNames().isEmpty());
    }

    @Test
    void buildCdLsCommandReturnsFullCdLsCommand() {

        String command = containerHelperService.buildCdPwdLsCommand("/src/path/here");
        assertThat(command == "cd /src/path/here;pwd;ls -la");
    }

    @Test
    void getImageDirectoriesReturnsDirectoryListing() throws IOException, InterruptedException {

        DirectoryListing getImageDirectories = containerHelperService.getImageDirectories("src", "8887ae791755");
        assertThat(getImageDirectories != null);

    }
}