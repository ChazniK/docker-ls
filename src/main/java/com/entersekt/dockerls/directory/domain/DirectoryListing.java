package com.entersekt.dockerls.directory.domain;

import java.util.List;

public class DirectoryListing {

    private String path;
    private List<AttributeInfo> attributeInfoList;

    public DirectoryListing(String path, List<AttributeInfo> attributeInfoList) {
        this.path = path;
        this.attributeInfoList = attributeInfoList;
    }

    public String getPath() {
        return this.path;
    }

    public List<AttributeInfo> getAttributeInfoList() {
        return this.attributeInfoList;
    }
}
