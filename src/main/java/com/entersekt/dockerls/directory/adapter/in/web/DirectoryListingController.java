package com.entersekt.dockerls.directory.adapter.in.web;

import com.entersekt.dockerls.directory.application.service.DirectoryListingService;
import com.entersekt.dockerls.directory.domain.DirectoryListing;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.core.Response;
import java.awt.desktop.PreferencesEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@RestController
@RequestMapping("/directories")
public class DirectoryListingController {

    private DirectoryListingService directoryListingService;

    public DirectoryListingController(DirectoryListingService directoryListingService) {
        this.directoryListingService = directoryListingService;
    }

    @GetMapping(value = "/{path}")
    public String getDirectoryListing(@PathVariable String path) throws IOException, InterruptedException {

        DirectoryListing listing = RestPreconditions.checkFound(directoryListingService.getDirectoryListing(path));

        if (listing == null) {
            return Response.status(Response.Status.NOT_FOUND).toString();
        }
        ObjectMapper mapper = new ObjectMapper();
        String response = mapper.writeValueAsString(listing);

        return response;
    }
}