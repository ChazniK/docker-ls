package com.entersekt.dockerls.directory.application.port.in;

import com.entersekt.dockerls.directory.domain.DirectoryListing;

import java.io.IOException;
import java.util.List;

public interface DirectoryListingQuery {

    DirectoryListing getDirectoryListing(String path) throws IOException, InterruptedException;
}
