package com.entersekt.dockerls.directory.domain;

public class ContainInfo {

    private String id;
    private String image;
    private String ports;
    private String names;

    public ContainInfo(String id, String image, String ports, String names) {
        this.id = id;
        this.image = image;
        this.ports = ports;
        this.names = names;
    }

    public String getId() {
        return this.id;
    }

    public String getImage() {
        return this.image;
    }

    public String getPorts() {
        return this.ports;
    }

    public String getNames() {
        return this.names;
    }
}
