package com.entersekt.dockerls.directory.application.service;

import com.entersekt.dockerls.directory.domain.AttributeInfo;
import com.entersekt.dockerls.directory.domain.ContainInfo;
import com.entersekt.dockerls.directory.domain.DirectoryListing;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class ContainerHelperService {

    private final String pattern = "([a-zA-Z-0-9:.]+)";
    private Pattern patternBuilder = Pattern.compile(pattern);


    public ContainInfo getDockerImage() throws InterruptedException, IOException {

        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command("docker", "ps").toString();

        Process process = processBuilder.start();

        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream(), "UTF8"));

        String line;
        ContainInfo container = null;

        while ((line = reader.readLine()) != null) {
            if (line.contains("8080/tcp")) {
                String[] lineFormatted = StringUtils.normalizeSpace(line).split(" ");
                container = new ContainInfo(lineFormatted[0], lineFormatted[1], lineFormatted[10], lineFormatted[11]);
            }
        }

        int exitCode = process.waitFor();
        System.out.println("\nExited with error code : " + exitCode);

        return container;
    }

    public DirectoryListing getImageDirectories(String fullPath, String dockerId) throws IOException, InterruptedException {
        ProcessBuilder processBuilder = new ProcessBuilder();
        String cdPwdLsCommand = buildCdPwdLsCommand(fullPath);
        processBuilder.command("docker", "exec", dockerId, "bash", "-c", cdPwdLsCommand);

        Process process = processBuilder.start();

        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream(), "UTF8"));

        List<AttributeInfo> attributeInfoList = new ArrayList<>();
        String path = reader.readLine().strip();

        int counter = 1;

        String line;

        while ((line = reader.readLine()) != null) {
           if (line.contains("No such file")) {
               return null;
           }
           else {
               if (counter >= 2) {
                   String[] lineFormatted = StringUtils.normalizeSpace(line).split(" ");
                   AttributeInfo attributeInfo = new AttributeInfo(lineFormatted[0], lineFormatted[1], lineFormatted[4], lineFormatted[8]);
                   attributeInfo.setDate(lineFormatted[5], lineFormatted[6], lineFormatted[7]);
                   attributeInfoList.add(attributeInfo);
                   String a = "";
               }
               counter += 1;
           }
        }

        int exitCode = process.waitFor();
        System.out.println("\nExited with error code : " + exitCode);

        return new DirectoryListing(path, attributeInfoList);
    }

    public String buildCdPwdLsCommand(String fullPath)
    {
        StringBuilder sb = new StringBuilder();

        if (fullPath == null || fullPath == "")
        {
            return null;
        }

        sb.append("cd").append(" ").append(fullPath).append(";").append("pwd").append(";").append("ls -la");

        return sb.toString();
    }
}
